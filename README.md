
# GitLab Notes
Taken from the Udemy course [GitLab CI: Pipelines, CI/CD and DevOps for Beginners](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/Wo)


## Configuration of a CI Project in GitLab
To get started with GitLab's CI capabilities, use the following guide.

### Step 1- Create a config file:
  - Create a `.gitlab-ci.yml` file in the root directory of your 	GitLab project
  - This config file will define your GitLab CI job
  

### Step 2- Define the steps of your job using YAML
  - Each job must contain *at least* one script to be executed during its runtime.
  - YAML stands for "YAML Ain't Markup Language" - it's a data serialization language used to run programs in a plaintext manner and is used by GitLab CI to configure the steps run during a deployment's phases (build, test, etc).
  - YAML uses **2 spaces** - not a 2-space-tab - to indent the nested levels of code. Spaced-indentation-formatting is how YAML   files are interpreted, so proper code formatting is essential.
  - a `.gitlab-ci.yml` will run in several defined stages. The most common stages are build, test, and run
  - see [this file](.\samples\build_a_car.gitlab-ci.yml) for a sample GitLab configuration file

### Step 3 
- set any environment variables (passwords, secrets, domains, etc.) to be used by the runner
- Environment variable settings can be found under the CI/CD menu

![Environment Variable Location](./images/environment_variables_location-.png)

### Step 4 - Test your configuration file
  - Commit your changes to the `.gitlab-ci.yml` to the GitLab repo. A guide to using Git in a command line tool can be found [here](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
  - After committing, go to the Pipline section of the  CI/CD menu
  
   ![CI/CD Menu screenshot](./images/pipeline_location_in_menu.png)
  - Select the most recent build to view its progress 
  
  - Alternatively, you can click the clock icon next to the commit number on the 'Project Overview' page to go see the most recent build in progress.
  
  ![Alternate Pipeline](./images/pipeline_building_icon.png)




## GitLab Architecture and Terminology

1) GitLab Server - contains your repositories and databases for a project.

2) GitLab Pipeline - managed and configured inside the GitLab Server, but **delegated** to a GitLab Runner. 

3) GitLab Runner - runs jobs delegated to it by the Server via the Pipeline. Will performs steps assigned in the job configuration files and also saves any artifacts created as part of the job. The Runner performs the following steps every time it is initialized by the server:

        a. Runs a Docker image to perform the steps of the  the job 
        b. Initializes an empty Git repo in the image 
        c. Clones the repo specified in the config file into the empty repo created in step 3b 
        d. Runs the steps outlined in the config file
        e. Zips any artifacts created in the steps and uploads them to the coordinator (in this case: the GitLab server). The artifacts are NOT committed to the project repository - they can only be accessed via the build menu for a project.
        f. The job either succeeds or fails; regardless of what happens the Docker image is destroyed, freeing up the Runner to perform another job. Nothing that occurred during run-time can be recovered. 
        
      _**Step 3f** is why it is so important to log everything you think you might need for future reference to an artifact_
    - Runners are configured in Project -> Settings -> CI/CD -> Runners -> "Expand"
    - Projects used shared Runners by default. All users are alloted up to 8 shared Runners on a free plan. 
    - You can also set up specific runners for specific projects and provision more runners depending on a project's needs (GitLab integrates very well with Kubernetes deployments to provide additional runners)
4)








## GitLab Notes Glossary

*job* : a series of steps defined in the `gitlab-ci.yml` file. Will be performed every time a build is triggered. 

*stage* : a specific *job* that is run during a project's build. The three most common stages are the *build stage*, *test stage*, and *run stage*. If no stages are defined, job items will be assigned to the *test* stage by default.





## GitLab Config/YAML operators
Most UNIX commands are applicable here (*cd*, *ls*, *pwd*, etc.).

See the [GitLab documentation](https://docs.gitlab.com/ee/ci/yaml/README.html) for the full suite of `.gitlab-ci.yml` options. 

What follows is a quick cheat sheet of commands.

#### Key:
- `keyword` --> Code text without any qualifiers is a keyword command and performs a defined action as a UNIX operator, program, or command
- `[]` --> square brackets in code text indicates a variable string or integer input that can be anything except for a `keyword`
- `[flags]` --> indicates any valid flag option (including no flags)
- `()` --> parenthesis indicate an optional portion of a command


### Data Read/Write Manipulation
- `"[text]" > [filename]` --> *write file*: overwrite `filename` contents with `text`
- `"[text]" >> [filename]` --> *append file*: appends `text` to `filename` contents
- `curl [data_source]` --> *curl command*: download or upload data from a given source for testing, use, etc.
- `curl [datasource] | tac | tac | grep -[flags] [string])`--> *tac program*: reads the entire output from `curl` and reverses the line order. You have to run it twice to get information back in the intial order (once to reverse the order, once to reverse the reverse). Because it has to read the whole input to read the last line, it will not output anything to `grep` until the `curl` is finished. `grep` will still close the read stream when it returns a positive result but the closing of the read stream only affects `tac` - which doesn't omit an error.

### Command Threading/Order of Operations
These commands are particularly useful for tests.

- `[command_1] | [command_2] (| [command_n]) ` --> *pipe command*: takes the output of `command_1` and passes it as input to `command_2` which can pass its output as an input ad infinitum until `command_n`. 
- `[command] &` --> *background execution command*: executes `command` and proceeds to next line of code.
- `sleep [number]` --> *timeout command*: pauses the execution of the script for `number` seconds (**NOT** milliseconds). Not best practice.


## Unorganized Notes as I go through the course (will be cleaned up)
 - You want to assign the 'build' output to an artifact for use by GitLab later on in a deployment
 - to use a Docker image in a job, use the `image` keyword. The name of the image in the in the tag is used in a `docker pull <image_name>` command to load that image into the Runner
 ```
 (.gitlab-ci.yml)

 do the thing:
  image: node
  script:
    - npm install
    - yarn build
```
- the image can be any valid image used by Docker. 

